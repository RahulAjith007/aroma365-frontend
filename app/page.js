'use client'

import HeadLayout from '@/layout/headlayout';
import { Box, Stack, TextField } from '@mui/material';
import React from 'react';
import classes from './login.module.css'
import ButtonPrimary from '@/components/ui/buttons/buttonPrimary';
import { useRouter } from 'next/navigation';

const Login = () => {

    const router = useRouter()

    return (
        // <HeadLayout>
            <Stack className='section-container' my={15} alignItems={'center'} justifyContent={'center'}>
                <Box className={classes.loginBox}>

                <img 
                    style={{display: 'block', margin: 'auto'}}
                    height={170}
                    width={170}
                    src={'/assets/images/logo.png'}
                />


                    <Stack 
                        spacing={2}
                        direction={'column'} 
                        alignItems={'center'} 
                        justifyContent={'center'}
                    >
                        {/* <label>Email</label> */}
                        <TextField 
                            className={classes.textInput}
                            id="outlined-basic" 
                            label="Enter Email Address" 
                            variant="standard" 
                        />
                        <TextField 
                            className={classes.textInput}
                            id="outlined-basic" 
                            type='password'
                            label="Enter Password" 
                            variant="standard" 
                        />
                        <ButtonPrimary 
                            handleClick={() => router.push('/home')}
                            style={{width: '100%', marginTop: 40}} 
                            title={'Login'}
                        />
                    </Stack>
                </Box>
            </Stack>
        // </HeadLayout>
    )
}   

export default Login;