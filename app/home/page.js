'use client'

import Banner from '@/components/banner'
import HeadLayout from '@/layout/headlayout'
import Navbar from '@/layout/navbar'
import Image from 'next/image'
import 'bootstrap/dist/css/bootstrap.min.css';
import BestSellers from '@/components/home/bestSellers'
import TopTrending from '@/components/home/topTrending'
import { Stack } from '@mui/material'
import Showcase from '@/components/home/showcase'
import { Instagram } from '@mui/icons-material'
import classes from './home.module.css'
import ComingSoon from '@/components/home/comingSoon'


export default function Home() {
  return (
    // <HeadLayout>
    //   <Banner />

    //   <Stack py={5}>
    //     <BestSellers/>
    //   </Stack>


    //   <Stack py={5}>
    //     <Showcase/>
    //   </Stack>


    //   <Stack py={5}>
    //     <TopTrending/>
    //   </Stack>
    // </HeadLayout>

    <ComingSoon classes={classes}/>
  )
}
