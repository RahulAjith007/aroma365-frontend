/* eslint-disable @next/next/no-img-element */
import { Instagram, Message, Phone } from '@mui/icons-material';
import { Divider, Grid, Stack, Typography, useMediaQuery } from '@mui/material';
import Image from 'next/image';
import { useRouter } from 'next/navigation';
import React from 'react';
import { useMemo } from 'react';
import appRoutes from '../../config/appRoutes';
import { customColors, customFontSize } from '../../config/constants';
import classes from './footer.module.css'


const Footer = ({showLocationMap}) => {
    const screen620 = useMediaQuery('(max-width:620px)');
    const screen767 = useMediaQuery('(max-width:767px)');
    const screen550 = useMediaQuery ('(max-width:550px)');
    const screen408 = useMediaQuery ('(max-width:408px)');
    const screen1008 = useMediaQuery ('(max-width:1008px)');
    const screen1200 = useMediaQuery ('(max-width:1200px)');



    const routes = useMemo(() => Object.values(appRoutes).
    filter(item => item.meta && item.meta.footer && item.meta.footerTitle), []) 

    const router = useRouter()

    const openInNewTab = (url) => {
        if(!url) {
            return
        }
        window.open(url, '_blank');
    }


    const addr = 'In publishing and graphic design, Lorem ipsum is a<br/> placeholder text commonly used to demonstrate <br/>the visual form of a document or a typeface'

    return (
        <footer className={`${classes.footerContainer}`}>


        {showLocationMap ? 
            <Grid
                overflow={'hidden'} 
                className="section-container"
                rowSpacing={5}
                columnSpacing={5}
                pt={1} mb={3} container
            >

                <Grid width={'100%'} item lg={12} md={12} sm={12}>
                    <div
                        style={{textAlign: 'center'}}
                        data-aos-offset="250"
                        data-aos="fade-up"
                    >
                        <Typography
                            my={3}
                            mb={5}
                            width={'100%'}
                            color="white"
                            textAlign={'center'}
                            dangerouslySetInnerHTML={{__html: 'LOCATE <span className="primary-color">US</span>'}}
                            fontSize={screen550 ? 32 : ''}
                            fontWeight="bold"
                            variant={screen767 ? (screen550 ? '' : 'h4') : "h3"} 
                        />
                    </div>
                </Grid>


                <Grid item lg={12}>
                    <img
                        style={{height: '100%', width: '100%'}}
                        alt="rupee"
                        src={'/assets/images/maps.webp'}
                    />
                </Grid>


            </Grid> : null}


            <Stack 
                flexWrap={'wrap'}
                justifyContent={screen1008 ? 'flex-start' : 'space-between'} 
                direction={'row'} 
                alignItems="flex-start"
                columnGap={4} 
                rowGap={3}
                className={`${classes.footerBottom}  section-container`} 
                mt={5} 
                py={4}
            >
                <Stack minWidth={screen1200 ? '100%' : null} style={{marginLeft: 0}} spacing={2}>
                    <img 
                        alt="synctric_logo" 
                        width={200} 
                        height={45} 
                        src={'/assets/images/logo.png'}
                        style={{cursor: 'pointer'}}
                        onClick={() => router.push(appRoutes.home.path)}
                    />    

                    <Typography
                        variant='body1' fontWeight={500}
                        dangerouslySetInnerHTML={{__html: screen620 ? addr.replaceAll('<br/>', '') : addr}} 
                    />           
                </Stack>


                <Stack style={{marginLeft: 0}} spacing={2}>
                    <Typography mb={1}  variant='subtitle1' fontWeight={600}>
                        HELP
                    </Typography> 


                    <Typography 
                        variant='body1' 
                        fontWeight={500} 
                        style={{cursor: 'pointer'}}
                        onClick={() => router.push(appRoutes.products.synctric.path)}
                    >
                        Order Tracking
                    </Typography>         

                    <Typography 
                        variant='body1' 
                        fontWeight={500} 
                        style={{cursor: 'pointer'}}
                        onClick={() => router.push(appRoutes.products.livshop.path)}
                    >
                        FAQ     
                    </Typography>          

                    <Typography 
                        variant='body1' 
                        fontWeight={500} 
                        style={{cursor: 'pointer'}}
                        onClick={() => router.push(appRoutes.products.vidstatus.path)}
                    >
                        Privacy Policy
                    </Typography>     

                    <Typography 
                        variant='body1' 
                        fontWeight={500} 
                        style={{cursor: 'pointer'}}
                        onClick={() => openInNewTab('http://blog.synctric.io/')}
                    >
                        Terms & Conditions
                    </Typography>  
                </Stack>


                <Stack style={{marginLeft: 0}} spacing={2}>
                    <Typography mb={1}  variant='subtitle1' fontWeight={600}>
                        COMPANY
                    </Typography> 


                    <Typography 
                        variant='body1' 
                        fontWeight={500} 
                        style={{cursor: 'pointer'}}
                        onClick={() => router.push(appRoutes.about.path)}
                    >
                        About Us
                    </Typography>   
                        
                    <Typography 
                        variant='body1' 
                        fontWeight={500} 
                        style={{cursor: 'pointer'}}
                        onClick={() => router.push(appRoutes.contactUs.path)}
                    >
                        Contact Us
                    </Typography>         

                    <Typography 
                        variant='body1' 
                        fontWeight={500} 
                        style={{cursor: 'pointer'}}
                        onClick={() => router.push(appRoutes.privacyPolicy.path)}
                    >
                        Privacy Policy     
                    </Typography>   

                    <Typography 
                        variant='body1' 
                        fontWeight={500} 
                        style={{cursor: 'pointer'}}
                        onClick={() => router.push(appRoutes.termsAndCondition.path)}
                    >
                        Terms & Conditions  
                    </Typography>          
                </Stack>


                <Stack style={{marginLeft: 0}} spacing={2}>
                    <Typography mb={1}  variant='subtitle1' fontWeight={600}>
                        STORE
                    </Typography> 


                    <Typography 
                        variant='body1' 
                        fontWeight={500} 
                        style={{cursor: 'pointer'}}
                        onClick={() => router.push(appRoutes.about.path)}
                    >
                        New
                    </Typography>   
                        
                    <Typography 
                        variant='body1' 
                        fontWeight={500} 
                        style={{cursor: 'pointer'}}
                        onClick={() => router.push(appRoutes.contactUs.path)}
                    >
                        Skin Care
                    </Typography>         

                    <Typography 
                        variant='body1' 
                        fontWeight={500} 
                        style={{cursor: 'pointer'}}
                        onClick={() => router.push(appRoutes.privacyPolicy.path)}
                    >
                        Makeup 
                    </Typography>   

                    <Typography 
                        variant='body1' 
                        fontWeight={500} 
                        style={{cursor: 'pointer'}}
                        onClick={() => router.push(appRoutes.termsAndCondition.path)}
                    >
                        Sale
                    </Typography>          
                </Stack>

            </Stack>

            
            <Stack className='section-container'>
                <Divider style={{height: 1}} color='white'/>
                <Stack direction={'row'} alignItems={'center'} justifyContent={'center'} mt={1} mb={1}>
                    <Typography 
                        style={{fontSize: 13.5}}
                        textAlign={'center'} variant='body2'
                    >
                        Copyright © {new Date().getFullYear()} <span style={{color: customColors.primary}}>Aroma365</span>
                    </Typography>
                </Stack>
            </Stack>
        </footer>
    )
}

export default Footer;
