import { Button, Stack, Typography } from '@mui/material'
import React from 'react'
import classes from './navbar.module.css';
import { FavoriteBorder, LocalMallOutlined, PersonOutlined } from '@mui/icons-material';
import { useRouter } from 'next/navigation';

const Navbar = () => {

    const router = useRouter()

  return (
    <Stack 
        className={classes.container} 
        justifyContent={'space-between'} 
        direction={'row'}
    >
        <Stack width={'45%'} direction={'row'} spacing={2} alignItems={'center'}>
            <Typography>
                Home
            </Typography>

            <Typography>
                Products
            </Typography>

            <Typography>
                Special Offers
            </Typography>

            <Typography>
                New Arrivals
            </Typography>
        </Stack>


        <Stack width={'10%'} alignItems={'center'} justifyContent={'center'}>
            <img 
                height={170}
                width={170}
                src={'/assets/images/logo.png'}
            />
        </Stack>

        <Stack width={'45%'} direction={'row'} spacing={2} alignItems={'center'} justifyContent={'end'}>
            {/* <input
                className={classes.searchInput}
            /> */}
            <FavoriteBorder/>
            <LocalMallOutlined/>

            <Button 
                onClick={() => router.push('/login')} 
                className={classes.loginButton} 
                style={{borderRadius: 50}}
            >
                <Stack px={2} direction={'row'} spacing={1}>
                    <PersonOutlined />
                    <Typography>
                        Login
                    </Typography>
                </Stack>
            </Button>
        </Stack>
    </Stack>
  )
}

export default Navbar
