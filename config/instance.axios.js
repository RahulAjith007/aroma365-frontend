import axios from 'axios';
import { base_url } from './constants';

export const instance = axios.create({
    baseURL: `${base_url}/api`
})
