const appRoutes = {    
    home: {
        path: '/',
        name: 'home',
        meta: {
            title: 'HOME',
            menu: true,
        }
    },
    about: {
        path: '/about',
        name:'about',
        meta: {
            title: 'ABOUT',
            menu: true,
        }
    },
    products: {
        path: '/products',
        synctric: {
            path: '/products/synctric',
            name:'synctric-product',
        },
        livshop: {
            path: '/products/livshop',
            name:'livshop-product',
        },
        vidstatus: {
            path: '/products/vidstatus',
            name:'vidstatus-product',
        }
    },
    services: {
        path: '/services',
        name:'services',
        meta: {
            title: 'SERVICES',
            menu: true,
        }
    },
    ourWorks: {
        path: '/our-works',
        name:'ourWorks',
        meta: {
            title: 'OUR WORKS',
            menu: true,
        }
    },
    contactUs: {
        path: '/contact-us',
        name:'contactUs',
        meta: {
            title: 'CONTACT US',
            menu: true,
        }
    },
    privacyPolicy: {
        path: '/privacy-policy',
        name:'privacyPolicy',
        meta: {
            title: 'PRIVACY POLICY',
            menu: false,
        }
    },
    termsAndCondition: {
        path: '/terms-conditions',
        name:'termsAndCondition',
        meta: {
            title: 'TERMS & CONDITIONS',
            menu: false,
        }
    },
    refundPolicy: {
        path: '/refund-policy',
        name:'refundPolicy',
        meta: {
            title: 'REFUND POLICY',
            menu: false,
        }
    },
}

export default appRoutes;