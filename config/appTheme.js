import { createTheme, responsiveFontSizes } from "@mui/material";
import { customColors, customFontSize } from "./constants";

const lightTheme = responsiveFontSizes(createTheme({
    breakpoints: {
      values: {
        xs: 0,
        sm: 448,
        md: 767,
        lg: 991,
        xl: 1536,
      },
    },
    palette: {
      primary: {
        main: customColors.primary,
      },
      secondary: {
        main: customColors.secondary
      },
      text: {
        primary: customColors.textPrimary,
        secondary: customColors.textSecondary,
        disabled: customColors.textDisabled
      },
    },
    typography: {
      fontFamily: 'jost',
      h1: {
        fontSize: customFontSize.h1,
        fontWeight: 700,
      },
      h2: {
        fontSize: customFontSize.h2,
        fontWeight: 700,
      },
      h3: {
        fontSize: customFontSize.h3,
        fontWeight: 700,
      },
      h4: {
        fontSize: customFontSize.h4,
        fontWeight: 700,
      },
      h5: {
        fontSize: customFontSize.h5,
        fontWeight: 700,
      },
      h6: {
        fontSize: customFontSize.h6,
        fontWeight: 600,
      },
      subtitle1: {
        fontSize: customFontSize.text1,
        fontWeight: 500,
      },
      body1: {
        fontSize: customFontSize.text2,
        fontWeight: 600,
      },
      body2: {
        fontSize: customFontSize.text3,
        fontWeight: 500,
      },
      // subtitle2: {
      //   fontSize: customFontSize.subHeading,
      //   fontFamily:'jost'
      // },
    },
  }))

  export default lightTheme