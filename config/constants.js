export const base_url = 'http://127.0.0.1:8000'


export const customColors = {
    background: '#F7F5FF',
    background2: '#FFFFFF',
    background3: '#0A2540',
    background4: '#F1F9FF',
    gradient1: '#2990EE',
    gradient2: '#4F66E5',
    primary: '#068AE3',
    secondary: '#838383',
    
    success: '#34A853',
    error: '#EA4335',

    textPrimary: '#323232',
    textSecondary: '#838383',
    textDisabled: '#838383',

    outline: '#AEB3CD',

    border: '#3F3F3F',
    
    underline: '#FAC291'
}


export const customFontSize = {
    h1: 120,
    h2: 80,
    h3: 70,
    h4: 60,
    h5: 35,
    h6: 30,
    text1: 25,
    text2: 18,
    text3: 16,
}
