import React from 'react'
import classes from './banner.module.css'
import { Box, Typography } from '@mui/material'
import ButtonFilledRounded from '../ui/buttons/buttonFilledRounded'

export default function Banner() {
  return (
    <div className={classes.container}>
        <div className={classes.wrapper}>
            <Typography 
                textAlign={'center'} 
                color={'black'} 
                variant='h4' 
                dangerouslySetInnerHTML={{__html: 'Lorem Ipsum is simpstrthe 1500s, when an unknown <br/>printer took a galley of type and scr specimen book.'}}
                fontWeight={600}/>

            <Typography 
                mt={1}
                textAlign={'center'} 
                color={'black'} 
                variant='body1' 
                dangerouslySetInnerHTML={{__html: 'Lorem Ipsum is simply ndustry. Lorem Ipsum has been the industrys k a galley of type and scrambled it to make a type specimen book. It has survived not <br/>only five centuries, but also, remaining essentially unchanged. It was popularised in'}}
                fontWeight={500}/>

            <Box mt={3}>
                <ButtonFilledRounded text="Shop Now"/>
            </Box>
            
        </div>
    </div>
  )
}
