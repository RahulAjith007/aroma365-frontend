import { Instagram } from '@mui/icons-material';
import React from 'react';


const ComingSoon = ({classes}) => {

    const openInNewTab = (url) => {
        if(!url) {
            return
        }
        window.open(url, '_blank');
    }


    return (
        <main style={{height: '70vh'}}>
            <img 
                style={{display: 'block', margin: 'auto', marginTop: 10}}
                height={170}
                width={170}
                src={'/assets/images/logo.png'}
            />
    
            <div style={{paddingTop: 200}} className="size1 overlay1">
            <div className="flex-col-c-m p-l-15 p-r-15">
                <h3 style={{fontSize: 28}} className="l1-txt1 txt-center p-b-10">
                Under Maintenance
                </h3>
    
                <p style={{fontSize: 17, fontFamily: 'Poppins-Regular'}} className="m2-txt1 txt-center p-b-20">
                Our website is under maintenance, follow us for update now!
                </p>

                <div>
                    <div 
                        onClick={() => openInNewTab('https://instagram.com/aroma_365_?igshid=NTc4MTIwNjQ2YQ==')}
                        className={classes.socialIconsContainer}>
                        <Instagram
                            className={classes.socialIcon}
                        /> 
                    </div>
                </div>
            </div>
            </div>
        </main>
    )
}

export default ComingSoon;


