import CustomCard from '@/components/ui/card'
import { Grid, Stack, Typography } from '@mui/material'
import React from 'react'
import { Col, Row } from 'reactstrap'

export default function TopTrending() {
  return (
    <Stack className='section-container'>
        <Typography mb={3} textAlign={'CENTER'} variant='h4'>
            Top Trending Now
        </Typography>

        <Grid columnSpacing={1} container>
            <Grid lg={3} md={4} sm={12}>
                <CustomCard/>
            </Grid>

            <Grid lg={3} md={4} sm={12}>
                <CustomCard/>
            </Grid>

            <Grid lg={3} md={4} sm={12}>
                <CustomCard/>
            </Grid>

            <Grid lg={3} md={4} sm={12}>
                <CustomCard/>
            </Grid>
        </Grid>
    </Stack>
  )
}
