import ButtonFilledRounded from '@/components/ui/buttons/buttonFilledRounded';
import { Box, Stack, Typography } from '@mui/material';
import React from 'react';

const Showcase = () => {
    return (
        <Stack className='section-container'>
            <Stack direction={'column'} alignItems={'flex-start'}>
                <Typography variant='h4'>
                    Lorem ipsum dolor sit amet, 
                </Typography>
                <Typography variant='body1' textAlign={'right'}>
                    orem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
                </Typography>
            </Stack>
            <Stack className='section-container' spacing={1} direction="row">
                <Stack
                        p={5} 
                        spacing={1}
                        display={'flex'} 
                        flexDirection="column"
                        alignItems={'flex-end'} 
                        justifyContent={'center'} 
                        style={{background: "#D9D9D9"}} 
                        maxWidth={'100%'} 
                        maxHeight={'100%'}
                    >
                        <Typography variant='h4'>
                            Lorem ipsum dolor sit amet, 
                        </Typography>
                        <Typography variant='body1' textAlign={'right'}>
                            orem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
                        </Typography>
                        <ButtonFilledRounded/>
                    </Stack>
                <Stack spacing={1} direction={'column'}>
                    <Stack
                        p={5} 
                        spacing={1}
                        display={'flex'} 
                        flexDirection="column"
                        alignItems={'flex-end'} 
                        justifyContent={'center'} 
                        style={{background: "#D9D9D9"}} 
                        maxWidth={'100%'} 
                        height={489}
                    >
                        <Typography variant='h4'>
                            Lorem ipsum dolor sit amet, 
                        </Typography>
                        <Typography variant='body1' textAlign={'right'}>
                            orem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
                        </Typography>
                        <ButtonFilledRounded/>
                    </Stack>

                    <Stack
                        p={5} 
                        spacing={1}
                        display={'flex'} 
                        flexDirection="column"
                        alignItems={'flex-end'} 
                        justifyContent={'center'} 
                        style={{background: "#D9D9D9"}} 
                        maxWidth={'100%'} 
                        height={489}
                    >
                        <Typography variant='h4'>
                            Lorem ipsum dolor sit amet, 
                        </Typography>
                        <Typography variant='body1' textAlign={'right'}>
                            orem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.
                        </Typography>
                        <ButtonFilledRounded/>
                    </Stack>
                </Stack>
            </Stack>
        </Stack>
    )

}

export default Showcase