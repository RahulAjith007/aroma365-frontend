import React from 'react';
// import { Button } from 'reactstrap'
import classes from './button.module.css'
import { Button } from '@mui/material';

const ButtonFilledRounded = () => {
    return (
        <Button
            className={classes.button}
            color="primary"
      >
        Shop Now
      </Button>
    )
}

export default ButtonFilledRounded;