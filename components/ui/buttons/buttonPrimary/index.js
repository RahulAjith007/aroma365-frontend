import React from 'react';
// import { Button } from 'reactstrap'
import classes from './button.module.css'
import { Button } from '@mui/material';

const ButtonPrimary = ({title, style, outlined, handleClick}) => {
    return (
        <Button
            onClick={handleClick}
            style={{...style}}
            className={outlined ? classes.buttonOutline : classes.button }
            color="primary"
      >
        {title}
      </Button>
    )
}

export default ButtonPrimary;