import { Box, Stack, Typography } from '@mui/material';
import React from 'react';
import classes from './card.module.css'
import ButtonPrimary from '../buttons/buttonPrimary';

const CustomCard = () => {
    return (
        <Stack width={340}>
            <Stack px={1}>
                <Box height={340} width={340} style={{background: "#D9D9D9"}}/>
                <Stack direction={'row'} alignItems={'center'} spacing={0.2}>
                    <img style={{height: 15, width: 15}} src="/assets/images/star_dark.png" alt="" />
                    <img style={{height: 15, width: 15}} src="/assets/images/star_dark.png" alt="" />
                    <img style={{height: 15, width: 15}} src="/assets/images/star_dark.png" alt="" />
                    <img style={{height: 15, width: 15}} src="/assets/images/star_light.png" alt="" />
                    <img style={{height: 15, width: 15}} src="/assets/images/star_light.png" alt="" />
                </Stack>
                <Typography variant="h5">
                    Lorem ipsum dolor sit amet
                </Typography>
                <Typography mb={1} variant="body1">
                    $200
                </Typography>
                <ButtonPrimary outlined title={'Add to Cart'}/>
            </Stack>
        </Stack>
    )
}

export default CustomCard;
